﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Module5.Configuration.Locale;
using Module5.Configuration.Watching;

namespace Module5
{
    public class Program
    {
        //Call Watcher
        private static void Main(string[] args)
        {
            using (var watcher = new AdvancedWatcher())
            {
                watcher.StartWatching();
            }
        }
    }
}
