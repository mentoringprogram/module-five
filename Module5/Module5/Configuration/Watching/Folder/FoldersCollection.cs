﻿using System.Configuration;

namespace Module5.Configuration.Watching.Folder
{
    /// <summary>
    /// This class describe collections of directories that we watch
    /// </summary>
    public class FoldersCollection : ConfigurationElementCollection
    {
        public FoldersCollection()
        {
        }

        public FolderElement this[int index]
        {
            get => (FolderElement)BaseGet(index);
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(FolderElement serviceConfig)
        {
            BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new FolderElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FolderElement) element).Path;
        }

        public void Remove(FolderElement serviceConfig)
        {
            BaseRemove(serviceConfig.Path);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }
    }
}
