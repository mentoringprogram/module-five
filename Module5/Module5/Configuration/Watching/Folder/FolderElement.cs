﻿using System.Configuration;

namespace Module5.Configuration.Watching.Folder
{
    public class FolderElement : ConfigurationElement
    {
        public FolderElement() {}

        public FolderElement(string path)
        {
            Path = path;
        }

        [ConfigurationProperty("Path", IsRequired = true, IsKey = true)]
        public string Path
        {
            get
            {
                return (string)this["Path"];
            }

            set
            {
                this["Path"] = value;
            }
        }
    }
}
