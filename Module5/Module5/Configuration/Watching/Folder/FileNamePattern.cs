﻿using System.Configuration;

namespace Module5.Configuration.Watching.Folder
{
    public class FileNamePattern : ConfigurationElement
    {
        [ConfigurationProperty(nameof(Pattern))]
        public string Pattern => (string) this["Pattern"];
    }
}
