﻿using System.Configuration;

namespace Module5.Configuration.Watching.Modify
{
    public class AddDate : ConfigurationElement
    {
        [ConfigurationProperty("IsAddDate", IsRequired = true, IsKey = true)]
        public bool IsAddDate
        {
            get
            {
                return (bool)this["IsAddDate"];
            }
        }
    }
}
