﻿using System.Configuration;

namespace Module5.Configuration.Watching.Modify
{
    public class AddNumber : ConfigurationElement
    {
        [ConfigurationProperty("IsAddNumber", IsRequired = true, IsKey = true)]
        public bool IsAddNumber
        {
            get { return (bool) this["IsAddNumber"]; }
        }
    }
}
