﻿using System.Configuration;

namespace Module5.Configuration.Watching.Destination
{
    public class DestinationElement : ConfigurationElement
    {
        [ConfigurationProperty("Path", IsRequired = true, IsKey = true)]
        public string Path 
        {
            get => (string) this["Path"];
            set => this["Path"] = value;
        }
    }
}
