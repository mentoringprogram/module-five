﻿using System.Configuration;
using Module5.Configuration.Watching.Destination;
using Module5.Configuration.Watching.Folder;
using Module5.Configuration.Watching.Modify;

namespace Module5.Configuration.Watching
{
    /// <summary>
    /// In this class there are properties that return value in string format
    /// </summary>
    public class WatchingSection : ConfigurationSection
    {
        [ConfigurationProperty(nameof(FileNamePattern))]
        public FileNamePattern Pattern => (FileNamePattern) base["FileNamePattern"];

        [ConfigurationProperty("Folders", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(FoldersCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public FoldersCollection Folders => (FoldersCollection)base["Folders"];

        [ConfigurationProperty("Destination")]
        public DestinationElement Destination => (DestinationElement) base["Destination"];

        [ConfigurationProperty("AddDate")]
        public AddDate IsAddDate => (AddDate) base["AddDate"];

        [ConfigurationProperty("AddNumber")]
        public AddNumber IsAddNumber => (AddNumber) base["AddNumber"];
    }
}
