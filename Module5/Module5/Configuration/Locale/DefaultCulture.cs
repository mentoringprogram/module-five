﻿using System.Configuration;

namespace Module5.Configuration.Locale
{
    /// <summary>
    /// This class contains property that define the culture for the application
    /// </summary>
    public class DefaultCulture : ConfigurationElement
    {
        [ConfigurationProperty("value", IsRequired = true, IsKey = true)]
        public string value => (string) this["value"];
    }
}
