﻿using System.Configuration;

namespace Module5.Configuration.Locale
{
    /// <summary>
    /// This class defines the element from a section in the configuration file
    /// </summary>
    public sealed class LocaleSection : ConfigurationSection
    {
        [ConfigurationProperty(nameof(DefaultCulture))]
        public DefaultCulture DefaultCulture => (DefaultCulture) base["DefaultCulture"];
    }
}
