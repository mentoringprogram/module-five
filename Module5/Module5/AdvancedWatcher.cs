﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using Module5.Configuration.Locale;
using Module5.Configuration.Watching;
using Module5.Rsource;

namespace Module5
{
    /// <summary>
    /// This class describes how the program works when a file change occurs
    /// </summary>
    public class AdvancedWatcher : IDisposable
    {
        /// <summary>
        /// List of directories that we follow
        /// </summary>
        public List<string> WatchPaths
        {
            get { return _watchPaths; }
            set
            {
                if (value == null || value.Count == 0)
                {
                    throw new ArgumentException("The paths are empty");
                }
                _watchPaths = value;
            }
        }
        public string Destination { get; set; }

        private List<FileSystemWatcher> _watchers;
        private List<string> _watchPaths;
        private int counter;

        /// <summary>
        /// The method keeps track of directories and, if the file has changed, moves it
        /// </summary>
        public AdvancedWatcher()
        {
            var localeConfiguration = (LocaleSection)ConfigurationManager.GetSection("LocaleSection");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(localeConfiguration.DefaultCulture.value);
            counter = 0;
            var watchingSection = (WatchingSection)ConfigurationManager.GetSection("WatchingSection");
            Destination = watchingSection.Destination.Path;
            var a = watchingSection.Folders.Count;
             _watchPaths = new List<string>();

            for (var i = 0; i < a; i++)
            {
                _watchPaths.Add(watchingSection.Folders[i].Path);
            }
        }

        /// <summary>
        /// Events of files changes
        /// </summary>
        public void StartWatching()
        {
            _watchers = new List<FileSystemWatcher>();
            foreach (var path in WatchPaths)
            {
                var watcher = new FileSystemWatcher(path);
                watcher.Created += OnCreated;
                watcher.Renamed += OnRenamed;
                watcher.EnableRaisingEvents = true;
                _watchers.Add(watcher);
            }

            Console.WriteLine(strings.quit);
            while (Console.Read() != 'q') ;
        }

        /// <summary>
        /// Description of event
        /// </summary>
        private void OnCreated(object source, FileSystemEventArgs e)
        {
            var localeConfiguration = (LocaleSection)ConfigurationManager.GetSection("LocaleSection");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(localeConfiguration.DefaultCulture.value);
            Console.WriteLine(strings.foundNew);
            MoveFile(e);
        }

        /// <summary>
        /// Description of event
        /// </summary>
        private void OnRenamed(object source, FileSystemEventArgs e)
        {
            var localeConfiguration = (LocaleSection)ConfigurationManager.GetSection("LocaleSection");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(localeConfiguration.DefaultCulture.value);
            Console.WriteLine(strings.renamed);
            MoveFile(e);
        }

        /// <summary>
        /// Method moves modified files
        /// </summary>
        private void MoveFile(FileSystemEventArgs e)
        {
            var section = (WatchingSection)ConfigurationManager.GetSection("WatchingSection");
            var date = section.IsAddDate.IsAddDate;
            var orderNumber = section.IsAddNumber.IsAddNumber;
            var regEx = new Regex(section.Pattern.Pattern);

            if (regEx.IsMatch(e.Name))
            {
                File.Move(e.FullPath, 
                    $"{Destination}/{(date ? DateTime.Now.ToShortDateString(): string.Empty)}_" +
                    $"{(orderNumber ? counter.ToString() : string.Empty)}_" +
                    $"{e.Name}");
            }
            else
            {
                Console.WriteLine(strings.notMatch);
            }
        }

        public void Dispose()
        {
            foreach (var fileSystemWatcher in _watchers)
            {
                fileSystemWatcher.Dispose();
            }
        }
    }
}
