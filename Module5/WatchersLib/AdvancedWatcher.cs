﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WatchersLib.Rsource;

namespace WatchersLib
{
    public class AdvancedWatcher: IDisposable
    {
        public List<string> WatchPaths
        {
            get { return _watchPaths; }
            set
            {
                if(value == null || value.Count == 0)
                {
                    throw new ArgumentException("The paths are empty");
                }
                _watchPaths = value;
            }
        }

        public string Culture { get; set; }
        public string Destination { get; set; }

        private List<FileSystemWatcher> _watchers;
        private List<string> _watchPaths;


        public void StartWatching()
        {
            if (WatchPaths != null && !string.IsNullOrEmpty(Destination))
            {
                _watchers = new List<FileSystemWatcher>();
                foreach (var path in WatchPaths)
                {
                    var watcher = new FileSystemWatcher(path);
                    watcher.Created += OnCreated;
                    watcher.Renamed += OnRenamed;
                    watcher.EnableRaisingEvents = true;
                    _watchers.Add(watcher);
                }

                Console.WriteLine(strings.quit);
                while (Console.Read() != 'q') ;
            }
            else
            {
                throw new ArgumentException("You have NOT specified all properties");
            }
        }

        private void OnCreated(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            // Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");
            // File.Move(e.FullPath, $"{Destination}/{e.Name}");
           
            Console.WriteLine(strings.foundNew);
        }

        private void OnRenamed(object source, FileSystemEventArgs e)
        {

            // File.Move(e.FullPath, $"{Destination}/{e.Name}");
           
            Console.WriteLine(strings.renamed);
        }


        public void Dispose()
        {
            foreach (var fileSystemWatcher in _watchers)
            {
                fileSystemWatcher.Dispose();
            }
        }
    }
}
